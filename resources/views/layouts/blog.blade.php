<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Gameneis-Blog</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ ('template/assets/img/favicon.png') }}" rel="icon">
  <link href="{{ ('template/assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ ('template/assets/vendor/aos/aos.css') }}" rel="stylesheet">
  <link href="{{ ('template/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ ('template/assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
  <link href="{{ ('template/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ ('template/assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ ('template/assets/css/style.css') }}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: SoftLand - v4.7.0
  * Template URL: https://bootstrapmade.com/softland-bootstrap-app-landing-page-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex justify-content-between align-items-center">

      <div class="logo">
        <h1><a href="layouts.home">GameSky</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a href="layouts.home">Home</a></li>
          <li><a class="active" href="layouts.blog">Blog</a></li>
          <li><a href="layouts.pricing">Gallery</a></li>
          <li><a href="layouts.features">Features</a></li>
          <li><a href="layouts.contact">Contact Us</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section class="hero-section" id="hero">

    <div class="wave">

      <svg width="100%" height="355px" viewBox="0 0 1920 355" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g id="Apple-TV" transform="translate(0.000000, -402.000000)" fill="#FFFFFF">
            <path d="M0,439.134243 C175.04074,464.89273 327.944386,477.771974 458.710937,477.771974 C654.860765,477.771974 870.645295,442.632362 1205.9828,410.192501 C1429.54114,388.565926 1667.54687,411.092417 1920,477.771974 L1920,757 L1017.15166,757 L0,757 L0,439.134243 Z" id="Path"></path>
          </g>
        </g>
      </svg>

    </div>


  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Home Section ======= -->

    <section class="section">
      <div class="container">

        <div class="row justify-content-center text-center mb-5">
          <div class="col-md-5" data-aos="fade-up">
            <h2>Game E-Sport yang masuk di ASIAN Games</h2>
          </div>
        </div>

    <section class="section">
    <div class="container">
    <div class="col-md-6" data-aos="fade-right">
            <img src="{{ ('template/assets/img/undraw_svg_2.jpg') }}" alt="Image" class="img-fluid">
          </div>
      <div class="container">
            <h2 class="mb-4">1. PUBG Mobile</h2>
            <p class="mb-4">PUBG MOBILE adalah game seluler Battle Royale yang dibuat secara independen oleh Lightspeed & Quantum Studios dari Tencent Game yang dilisensikan secara resmi oleh PLAYERUNKNOWN'S BATTLEGROUNDS. Dirilis secara global pada Maret 2018, PUBG MOBILE diciptakan dengan Unreal Engine 4 dan berfokus pada kualitas visual, peta, pengalaman menembak, serta aspek lainnya untuk memberikan pengalaman Battle Royale yang nyata bagi para pemain. Seratus pemain akan mendarat di medan tempur untuk memulai perjalanan yang intens sekaligus menyenangkan. Selain mencari suplai untuk bertahan hidup, pemain juga harus memanfaatkan potensi maksimal dari medan peta, kendaraan, dan item untuk menjadi tim terakhir yang bertahan di medan tempur. Periksa detak jantungmu dan waspadalah saat zona bermain mulai menyusut. Berbagai peta tersedia untuk dipilih, mulai dari Erangel, Miramar, Sanhok, Livik, dan banyak lagi. Jangan lupakan ada beragam gameplay yang menunggu penjelajahanmu! Hingga saat ini, pengaruh PUBG MOBILE tidak hanya memenuhi tujuannya sebagai game, tetapi juga merupakan pusat yang menghubungkan pemain dari seluruh dunia sekaligus brand yang menginspirasi para pemain untuk hidup maksimal sesuai passion mereka.
PUBG MOBILE: Battle Royale asli, yang pertama dan terbaik!</p>
          </div>
          
        </div>
      </div>
    </section>

    <section class="section">
      <div class="container">
      <div class="col-md-6" data-aos="fade-right">
            <img src="{{ ('template/assets/img/undraw_svg_3.jpg') }}" alt="Image" class="img-fluid">
          </div>
            <h2 class="mb-4">2. Mobile Legends Bang-bang</h2>
            <p class="mb-4">Mobile Legends atau ML merupakan game MOBA yang dirilis oleh Moontoon. Game ini bisa dimainkan di ponsel Android maupun IOS. Dalam game MOBA ini akan ada 10 pemain yang akan dibagi menjadi 2 tim.
Aturan mainnya yakni 5 vs 5. Rata-rata permainan dalam 1 game yakni sekitar 15-20 menit. Setiap pemain bisa memilih 1 hero dari puluhan daftar hero yang tersedia.Untuk memainkannya, para pemain bisa menggunakan hero yang tersedia. Hero yang tersedia adalah hero yang telah dibeli dan hero yang telah disediakan secara otomatis oleh sistem. Setiap hero mempunyai skill masing-masing. Inilah keseruan Mobile Legends yaitu memaksimalkan kemampuan atau skill dari hero yang digunakan.

Untuk mendapatkan hero bisa dilakukan dengan beberapa cara, seperti Top Up diamond, atau dengan menggunakan ticket dan Battle Point. Selain skill, setiap hero juga mempunyai karakteristik masing-masing, di antaranya marksman, tank, mage, support, dan assassin. Mengenali setiap karakteristik ini juga jadi kunci dalam mengembangkan skill hero yang digunakan.</p>
          </div>
        </div>
      </div>
    </section>

    <!-- ======= Testimonials Section ======= -->
    <section class="section border-top border-bottom">
      <div class="container">
        <div class="row justify-content-center text-center mb-5">
          <div class="col-md-4">
            <h2 class="section-heading">Review From Our Users</h2>
          </div>
        </div>
        <div class="row justify-content-center text-center">
          <div class="col-md-7">

            <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
              <div class="swiper-wrapper">

                <div class="swiper-slide">
                  <div class="review text-center">
                    <p class="stars">
                      <span class="bi bi-star-fill"></span>
                      <span class="bi bi-star-fill"></span>
                      <span class="bi bi-star-fill"></span>
                      <span class="bi bi-star-fill"></span>
                      <span class="bi bi-star-fill muted"></span>
                    </p>
                    <h3>Excellent App!</h3>
                    <blockquote>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius ea delectus pariatur, numquam
                        aperiam dolore nam optio dolorem facilis itaque voluptatum recusandae deleniti minus animi,
                        provident voluptates consectetur maiores quos.</p>
                    </blockquote>

                    <p class="review-user">
                      <img src="{{ ('template/assets/img/person_1.jpg') }}" alt="Image" class="img-fluid rounded-circle mb-3">
                      <span class="d-block">
                        <span class="text-black">Jean Doe</span>, &mdash; App User
                      </span>
                    </p>

                  </div>
                </div><!-- End testimonial item -->

                <div class="swiper-slide">
                  <div class="review text-center">
                    <p class="stars">
                      <span class="bi bi-star-fill"></span>
                      <span class="bi bi-star-fill"></span>
                      <span class="bi bi-star-fill"></span>
                      <span class="bi bi-star-fill"></span>
                      <span class="bi bi-star-fill muted"></span>
                    </p>
                    <h3>This App is easy to use!</h3>
                    <blockquote>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius ea delectus pariatur, numquam
                        aperiam dolore nam optio dolorem facilis itaque voluptatum recusandae deleniti minus animi,
                        provident voluptates consectetur maiores quos.</p>
                    </blockquote>

                    <p class="review-user">
                      <img src="{{ ('template/assets/img/person_2.jpg') }}" alt="Image" class="img-fluid rounded-circle mb-3">
                      <span class="d-block">
                        <span class="text-black">Johan Smith</span>, &mdash; App User
                      </span>
                    </p>

                  </div>
                </div><!-- End testimonial item -->

                <div class="swiper-slide">
                  <div class="review text-center">
                    <p class="stars">
                      <span class="bi bi-star-fill"></span>
                      <span class="bi bi-star-fill"></span>
                      <span class="bi bi-star-fill"></span>
                      <span class="bi bi-star-fill"></span>
                      <span class="bi bi-star-fill muted"></span>
                    </p>
                    <h3>Awesome functionality!</h3>
                    <blockquote>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius ea delectus pariatur, numquam
                        aperiam dolore nam optio dolorem facilis itaque voluptatum recusandae deleniti minus animi,
                        provident voluptates consectetur maiores quos.</p>
                    </blockquote>

                    <p class="review-user">
                      <img src="{{ ('template/assets/img/person_3.jpg') }}" alt="Image" class="img-fluid rounded-circle mb-3">
                      <span class="d-block">
                        <span class="text-black">Jean Thunberg</span>, &mdash; App User
                      </span>
                    </p>

                  </div>
                </div><!-- End testimonial item -->

              </div>
              <div class="swiper-pagination"></div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- End Testimonials Section -->

    <!-- ======= CTA Section ======= -->

    </section><!-- End CTA Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer class="footer" role="contentinfo">
    <div class="container">
      <div class="row">
        <div class="col-md-4 mb-4 mb-md-0">
          <h3>About GameSky</h3>
          <p>Website yang menyediakan sofware maupun informasi tentang banyak games di kalangan gamer</p>
          <p class="social">
            <a href="#"><span class="bi bi-twitter"></span></a>
            <a href="#"><span class="bi bi-facebook"></span></a>
            <a href="#"><span class="bi bi-instagram"></span></a>
            <a href="#"><span class="bi bi-linkedin"></span></a>
          </p>
        </div>
        <div class="col-md-7 ms-auto">
          <div class="row site-section pt-0">
            <div class="col-md-4 mb-4 mb-md-0">
              <h3>Navigation</h3>
              <ul class="list-unstyled">
                <li><a href="layouts.pricing">Gallery</a></li>
                <li><a href="layouts.features">Features</a></li>
                <li><a href="layouts.blog">Blog</a></li>
                <li><a href="layouts.contact">Contact</a></li>
              </ul>
            </div>
            
            
          </div>
        </div>
      </div>

      <div class="row justify-content-center text-center">
        <div class="col-md-7">
          <p class="copyright">&copy; Copyright SoftLand. All Rights Reserved</p>
          <div class="credits">
            <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=SoftLand
          -->
            Designed by <a href="https://bootstrapmade.com/">Neisky</a>
          </div>
        </div>
      </div>

    </div>
  </footer>

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ ('template/assets/vendor/aos/aos.js') }}"></script>
  <script src="{{ ('template/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ ('template/assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
  <script src="{{ ('template/assets/vendor/php-email-form/validate.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ ('template/assets/js/main.js') }}"></script>

</body>

</html>