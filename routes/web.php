<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/layouts.home', function () {
    return view('layouts.home');
});

Route::get('/layouts.features', function () {
    return view('layouts.features');
});

Route::get('/layouts.pricing', function () {
    return view('layouts.pricing');
});

Route::get('/layouts.blog', function () {
    return view('layouts.blog');
});

Route::get('/layouts.contact', function () {
    return view('layouts.contact');
});